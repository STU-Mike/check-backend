'use strict';
module.exports = {
  schedule: {
    interval: '4s',
    type: 'worker',
  },
  async task(ctx) {
    const app = ctx.app;
    const url = app.api;
    const res = await ctx.curl(url.backend_url, {
      dataType: 'text',
    });
    if (res.data !== 'hi, egg') {
      // TODO:发送告警信息到企业微信
      console.log('发送告警信息');
    } else {
      console.log('backend已经成功开启');
    }
  },
};
