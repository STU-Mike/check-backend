'use strict';
const Redis = require('ioredis');
const authedRedis = new Redis(6379, '127.0.0.1');
module.exports = {
  schedule: {
    interval: '4s',
    type: 'worker',
  },
  async task() {
    // 根据redis的连接状态判断是否开启
    if (authedRedis.status !== 'ready') {
      // TODO:发送redis错误告警
      console.log('发送告警信息');
    } else {
      console.log('redis已经成功开启');
    }
  },
};
