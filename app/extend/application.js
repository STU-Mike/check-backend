'use strict';
const API = Symbol('Aplication#checkAPI');

module.exports = {
  get api() {
    if (!this[API]) {
      const config = this.config;
      this[API] = {
        oauth_url: config.oauth_base_url + '/home',
        backend_url: config.backend_base_url + '/home',
      };
    }
    return this[API];
  },
};
