/* eslint valid-jsdoc: "off" */

'use strict';

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  // 配置需要检查后台的基本路径
  const config = exports = {
    oauth_base_url: 'http://139.199.224.230:7001',
    backend_base_url: 'http://139.199.224.230:7002',
  };

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1557927920133_2591';

  // add your middleware config here
  config.middleware = [];

  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
  };

  return {
    ...config,
    ...userConfig,
  };
};
